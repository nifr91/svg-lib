class SVG
  def rect(*,x,y,width,height,title="",**args)
    @io.print %(<rect x="#{x}" y="#{y}" width="#{width}" height="#{height}" )
    print_args(**args)
    @io.print " >"
    print_title(title)
    @io.puts("</rect>")
  end 

  def line(*,x1,y1,x2,y2,title="",**args)
    @io.print %(<line x1="#{x1}" y1="#{y1}" x2="#{x2}" y2="#{y2}" )
    print_args(**args)
    @io.print " >"
    print_title(title)
    @io.puts("</line>")
  end

  def circle(*,cx,cy,r,title="",**args)
    @io.print %(<circle cx="#{cx}" cy="#{cy}" r="#{r}" )
    print_args(**args)
    @io.print " >"
    print_title(title)
    @io.puts("</circle>")
  end 

  def polygon(*,points,title="",**args)
    @io.print "<polygon "
    @io.print "points=\""
    points.each do |p|
      @io.print p[0],",",p[1]," "
    end 
    @io.print "\" "
    print_args(**args)
    @io.print " >"
    print_title(title)
    @io.puts "</polygon>"
  end 
  

  def path(*,title="",**args)
    @io.print %(<path d=")
    yield Path.new(self)
    @io.print %(" )
    print_args(**args)
    @io.puts %( >)
    print_title(title)
    @io.puts %(</path>)
  end 

  private class SVG::Path
    @svg : SVG
    def initialize(@svg)
    end 

    def m(x,y,relative=false)
      @svg.print %(#{relative ? "m" : "M"} #{x} #{y} )
    end 

    def l(x,y,relative=false)
      @svg.print %(#{relative ? "l" : "L"} #{x} #{y} )
    end

    def h(x,relative=false)
      @svg.print %(#{relative ? "h" : "H"} #{x} )
    end 

    def v(y,relative=false)
      @svg.print %(#{relative ? "v" : "V"} #{y} )
    end 

    def c(x1,y1,x2,y2,x,y,relative=false)
      @svg.print %(#{relative ? "c" : "C"} #{x1} #{y1} #{x2} #{y2} #{x} #{y} )
    end 

    def s(x2,y2,x,y,relative=false)
      @svg.print %(#{relative ? "s" : "S"} #{x2} #{y2} #{x} #{y} )
    end 

    def q(x1,y1,x,y,relative=false)
      @svg.print %(#{relative ? "q" : "Q"} #{x1} #{y1} #{x} #{y} )
    end 

    def t(x,y,relative=false)
      @svg.print %(#{relative ? "t" : "T"} #{x} #{y} )
    end 

    def a(rx,ry,x_axis_rot,large_arc_flag,sweep_flag,x,y,relative=true)
      @svg.print %(#{relative ? "a" : "A"} #{rx} #{ry} #{x_axis_rot} #{large_arc_flag} #{sweep_flag} #{x} #{y} )
    end 

    def z()
      @svg.print %(z )
    end 
  end 
end 


