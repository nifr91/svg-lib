class SVG
  private class Transform
    @io : IO
    def initialize(@io)
    end 

    def translate(x,y)
      @io.print %(translate(#{x} #{y}) )
      self
    end 

    def rotate(d,x=nil,y=nil)
      @io.print %(rotate(#{d} #{x} #{y}) )
      self
    end 

    def scale(x,y=nil)
      @io.print %(scale(#{x} #{y}) )
      self
    end 

    def skewx(a)
      @io.print %(skewX(#{a}) )
      self
    end 
    
    def skewy(a)
      @io.print %(skewY(#{a}) )
      self
    end 

    def matrix(a,b,c,d,e,f)
      @io.print %(matrix(#{a} #{b} #{c} #{d} #{e} #{f}) )
      self
    end 
  end 

    def self.transform
      String.build do |io|
        yield Transform.new(io)
      end 
    end
end 
