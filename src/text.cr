class SVG
  def text(*,x,y,text,title="",**args)
    @io.print %(<text x="#{x}" y="#{y}" )
    print_args(**args)
    @io.print " >"
    print_title(title)
    @io.print text
    @io.puts "</text>"
  end 
end 
