class SVG
  property width
  property height
  def initialize(@io : IO,@width : Float64,@height : Float64)
  end 
  def puts(*args)
    @io.puts(*args)
  end 
  def print(*args)
    @io.print(*args)
  end 
  def self.new(x,y,width,height,io=STDOUT,**args)
    svg = SVG.new(io,width,height)
    svg.print %(<svg viewBox="#{x} #{y} #{width} #{height}" version="1.1" xmlns="http://www.w3.org/2000/svg" )
    svg.print_args(**args)
    svg.print(" >")
    yield svg
    svg.puts "</svg>"
  end
  def print_args(**args)
    args.each do |k,v|
      @io.print k.to_s.gsub("_","-"),"=","\"",v,"\""," "
    end 
  end
  def print_title(title)
    @io.puts %(<title>#{title}</title>) if title != ""
  end 
end 

require "./shapes.cr"
require "./text.cr"
require "./containers.cr"
require "./transform.cr"
require "./plot2d.cr"
