# SVG\_LIB

Este es un modulo simple para crear archivos SVG empleando el lenguaje 
[crystal-lang].

La idea principal es contar con una librería en el lenguaje que 
permita generar imágenes de alta calidad, mediante una interfaz sencilla
a SVG. 


##  Primeros pasos 

### Requisitos Previos 

Los requisitos para utilizar la librería son: 

* Crystal 0.35.1 
* OS Linux (probado en Manjaro)

### Instalación 

#### Clonando el repositorio 

Para instalar la librería se puede copiar directamente el repositorio: 

```sh
git clone https://gitlab.com/nifr91/svg-lib.git svg-lib
```

Finalmente se debe hacer referencia al archivo `src/svg-lib.cr` en 
la aplicación e.g

```crystal 
# myapp.cr 
require "./svg-lib/src/svg-lib.cr"
```

#### Shards

Alternativamente se puede instalar empleando el gestor de 
dependencias [Shards]. Para ello se añade la dependencia al archivo 
`shard.yml`

```yaml
# shards.yml 
dependencies:
  svg-lib: 
    git: https://gitlab.com/nifr91/svg-lib.git
    branch: master
```

se instala ejecutando el comando 

```sh 
shards install 
```

Finalmente se incluye la librería en la aplicación e.g

```crystal
# myapp.cr 
require "svg-lib" 
```

### Uso 

Se tiene la intención que el crear una imagen empleando el módulo sea 
similar a crear un SVG. Para cada elemento del SVG se tiene genera una 
instancia de la clase que lo representa. 
 

Por ejemplo el código SVG: 

```html 
<svg viewBox="0 0 10 10" xmls="https://www.w3.org/2000/svg/" version="1.1" > 
<circle fill="black" cx="5" cy="5" r="2"></circle>
<svg> 
```

Corresponde a el código en Crystal: 

```crystal 
SVG.build(viewBox: ViewBox.new(0.0,0.0,10.0,10.0)) do |svg| 
  svg.push Circle.new(cx: 5.0, cy: 5.0, r: 2.0,fill: Color::BLACK) 
end 
```

#### Código mínimo 

El código mínimo que crea un SVG con un circulo y lo imprime a STDOUT es :

```crystal
# example.cr 

require "svg-lib"
include SVG_LIB

STDOUT << SVG.build(viewBox: ViewBox.new(0.0,0.0,10.0,10.0)) do |svg|
  svg.push Circle.new(cx: 5.0, cy: 5.0, r: 2.0,fill: Color::BLACK) 
end 
```

#### Ejemplo 

El siguiente código genera un icono para la librería: 

```crystal
# example.cr 
require "svg-lib"

include  SVG_LIB

svg = SVG.build(viewBox: ViewBox.new(0.0,0.0,50.0,50.0)) do |svg|

  svg.push Path.new(stroke: Color::BLACK, fill: Paint::NONE).
    absolute.move(x:10.0,y:5.0).h_line(34.0).line(x: 40.0, y: 11.0).
    v_line(45.0).h_line(10.0).close
  
  svg.push Path.new(stroke: Color::BLACK,stroke_width: 0.25,fill: Color::BLACK).
    absolute.move(x: 34.0, y: 5.0).relative.line(x: 6.0, y: 6.0).h_line(-6.0).
    close

  svg.push Text.new(text: "SVG_LIB", x: 25.0, y: 16.0,
    font_size: 5.0, text_anchor: Text_anchor::MIDDLE)

  svg.push( G.build(transform: Transform.new.translate(x: 15.0, y: 32.0)) do |g|
    line_style = { stroke: Color::GRAY, stroke_width: 0.5, 
      stroke_dasharray: Stroke_dasharray.new(1,1)} 

    p1,p2 = {9.0, -10.0},{13.0, 10.0} 
    sp,ep = {0.0,0.0},{20.0,0.0} 

    g.push Line.new(**line_style,
      x1: sp[0], y1: sp[1], x2: p1[0], y2: p1[1]) 
    g.push Line.new(**line_style,
      x1: ep[0], y1: ep[1], x2: p2[0], y2: p2[1]) 

    g.push Path.new(stroke: Color::BLACK, fill: Paint::NONE).
      absolute.move(x: sp[0],y: sp[1]). 
      c_curve(x1: p1[0], y1: p1[1], x2: p2[0], y2: p2[1], x: ep[0], y: ep[1])

    g.push Circle.new(cx: p1[0], cy: p1[1], r: 1, fill: Color::GRAY )
    g.push Circle.new(cx: p2[0], cy: p2[1], r: 1, fill: Color::GRAY )
    g.push Circle.new(cx: sp[0], cy: sp[1], r: 1 ) 
    g.push Circle.new(cx: ep[0], cy: ep[1], r: 1 )

  end)

end 

svg.save "image.svg"
```
y da como resultado 

![image.svg][image.svg]

## Autor 

Ricardo Nieto Fuentes

nifr91@gmail.com 

## Licencia 

El proyecto esta licenciado la licencia MIT - ver [LICENSE.md] para mas 
detalles


[//]: # "Enlaces -----------------------------------"

[crystal-lang]: https://crystal-lang.org

[LICENSE.md]: ./LICENSE.md 

[image.svg]: ./examples/images/icon.svg

[Shards]: https://github.com/crystal-lang/shards.git 
