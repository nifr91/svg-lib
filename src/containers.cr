class SVG
  def defs
    @io.puts "<defs>"
    yield self
    @io.puts "</defs>"
  end 

  def marker(*,id,viewBox,refX,refY,title="",**args)
    @io.print %(<marker id="#{id}" viewBox="#{viewBox.join(" ")}" refX="#{refX}" refY="#{refY}" )
    print_args(**args)
    @io.puts " >"
    print_title(title)
    yield self
    @io.puts "</marker>"
  end 

  def g(**args)
    @io.print %(<g )
    print_args(**args)
    @io.puts %( >)
    yield self
    @io.puts %(</g>)
  end 
end 
