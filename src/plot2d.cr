class SVG
  def plot 
    return Plot.new
  end 

  class Plot
    @svg : SVG
    @@colors : Array(String) = [
      "cyan","red","green","blue","orange","deeppink","lightgreen","black","purple","gray",
      "gray","cyan","purple","tomato","black","green","lightgreen","cornflowerblue","pink","orange"] * 2
    
    def self.colors() 
      return @@colors 
    end 
    @@markers : Array(String) = [".","o","s","1","*","x","+","d","p","H"] * 4
    def self.markers() 
      return @@markers 
    end 
    @@linestyles : Array(String) = [
      "-","--","-.",":","3","10 1","10 5","5 10","5 2 2 5","2 10",
      "2 10","5 2 2 5","5 10","10 5","10 1","3",":","-.","--","-"
    ] * 2
    def self.linestyles
      return @@linestyles
    end 
   
    property minx : Float64 = -1.0/0.0 
    property maxx : Float64 = 1.0/0.0
    property miny : Float64 = -1.0/0.0
    property maxy : Float64 = 1.0/0.0

    def minmax(x,y,xlog = false,ylog = false)
      miny,maxy = y.flatten.minmax
      @miny = miny unless @miny.finite?
      @maxy = maxy unless @maxy.finite?

      if ylog 
        @miny = Math.log(Math.max(1e-12,@miny),10)
        @maxy = Math.log(@maxy,10)
      end 

      minx,maxx = x.flatten.minmax 
      @minx = minx unless @minx.finite?
      @maxx = maxx unless @maxx.finite?

      if xlog 
        @minx = Math.log(Math.max(1e-12,@minx),10)
        @maxx = Math.log(@maxx,10)
      end 

    end 

    property padleft   = 100
    property padright  = 20
    property padtop    = 50
    property padbottom = 80
    property colors    = [] of String
    property labels    = [] of String
    property markers   = [] of String
    property linestyles   = [] of String
    property opacities = [] of Float64
    property linewidth = 2

    property markernum  = 10
    property markersize = 15
    
    property xlabel = ""
    property ylabel = ""
    property axcolor = "black"
    property axlinewidth = 2
    property axlabelstrokewidth = 0.5
    property axlabelfontsize = 20
    
    property ticksnum = 5
    property tickslen = 10 
    property tickstextlen = 60
    property ticksfontsize = 12
    property ticksstrokewidth = 0.1
   
    property legendfontsize = 10 
    property legendbgcolor = "white"
    property legendbgopacity = 0.5
    property legendfgcolor = "black"
    property legendlinelen = 50 
    property legendheight = 100 
    property legendpad = 10 

    property title = ""
    property titlefontsize = 20 

    def initialize(@svg)
    end 

    def self.mapxcoords(x,minx,maxx,w,padleft,padright,log)
      x = log ? Math.log(x,10) : x
      xnew = padleft + ((x - minx)/((maxx-minx) + 1e-100))*(w-(padleft + padright))
    end 

    def self.mapycoords(y,miny,maxy,h,padtop,padbottom,log)
      y = log ? Math.log(y,10) : y
      ynew = padtop + ((maxy - y)/((maxy-miny) + 1e-100))*(h-(padtop + padbottom))
    end

    def map_and_zip(xraw,yraw,minx,maxx,miny,maxy,xlog,ylog)
      xp = xraw.map{|v| Plot.mapxcoords(v,minx,maxx,@svg.width,@padleft,@padright,xlog)}
      yp = yraw.map{|v| Plot.mapycoords(v,miny,maxy,@svg.height,@padtop,@padbottom,ylog)}
      
      return xp.zip(yp) 
    end 

    def set_plot_style(label,color,marker,linestyle,opacity)
      label = @labels.size.to_s if !label
      color = @@colors[@labels.size] if  !color 
      marker = @@markers[@labels.size] if !marker
      linestyle = @@linestyles[@labels.size] if !linestyle
      opacity = 1.0 if !opacity

      @labels.push(label)
      @colors.push(color)
      @markers.push(marker)
      @linestyles.push(linestyle)
      @opacities.push(opacity)
    end 

    def scatter(
      xraw, yraw, 
      minx = @minx, maxx = @maxx, miny = @miny, maxy = @maxy,
      label = nil, 
      color = nil, 
      marker = nil, 
      opacity = nil,
      xlog = false, ylog=false,**args)
     
      self.set_plot_style(label,color,marker,"",opacity)
      points = self.map_and_zip(xraw,yraw,minx,maxx,miny,maxy,xlog,ylog)
  
      @svg.g(id: @labels.last, stroke: "none",fill: @colors.last,opacity: @opacities.last) do |g|
        points.each_with_index do |(x,y),i|
          self.marker(@markers.last,x,y,"%g" % yraw[i])
        end 
      end 
    
    end 

    def plot(
      xraw, yraw,
      minx = @minx,maxx = @maxx,miny = @miny,maxy = @maxy,
      label = nil,
      color=nil,
      marker=nil,
      linestyle=nil,
      opacity = nil,
      xlog=false,ylog=false)

      self.set_plot_style(label,color,marker,linestyle,opacity)
      points = self.map_and_zip(xraw,yraw,minx,maxx,miny,maxy,xlog,ylog)
      
      @svg.g(
        id: @labels.last,
        stroke: @colors.last, 
        fill: @colors.last, 
        stroke_width: @linewidth,
        opacity: @opacities.last) do |g|

        g.path(title: @labels.last,fill: "none",stroke_dasharray: self.linestyle(linestyle)) do |path|
          missing = true
          points.each do |(x,y)|
            if !x.finite? || !y.finite?
              missing = true 
              next
            end 
            if missing 
              path.m(x,y)
              missing = false
            end 
            path.l(x,y) 
          end 
        end 
      
        g.g(id: @labels.last+"-markers", fill: @colors.last) do |mg| 
          points.each_with_index do |(x,y),i|
            next if ((i % (points.size // @markernum)) != 0) || !x.finite? || !y.finite?
            self.marker(@markers.last,x,y,yraw[i].round(2))
          end 
        end 
      end  

    end

    def title
      @svg.g(id: "title",text_anchor: "middle") do |g| 
        g.text(
          x: (@svg.width) //2, y: @titlefontsize,
          text: @title,
          font_size: @titlefontsize)
      end 
    end 

    def legend
      legendwidth  = @legendlinelen + 2*@legendpad + @legendfontsize*@labels.map(&.size).max
      legendheight = 2*@legendpad + @labels.size*2*@legendfontsize

      t = SVG.transform do |t|
        t.translate(@svg.width-legendwidth-@padright,0)
      end 

      @svg.g(id: "legend",transform: t) do |g|
        g.rect(
          x: 0, 
          y: 0 ,
          width: legendwidth, 
          height: legendheight, 
          stroke: "none" , 
          fill: @legendbgcolor, 
          opacity: @legendbgopacity)

        @labels.size.times do |i|
          y = (i+1)*(2*@legendfontsize)
          g.g(id: "legend-marker-line-"+@labels[i],fill: @colors[i],stroke: @colors[i]) do |g|
            g.line(
              x1: @legendpad,y1: y-@legendfontsize//4, 
              x2: @legendpad+@legendlinelen, y2: y-@legendfontsize//4,
              stroke_dasharray: self.linestyle(@linestyles[i]))
            self.marker(@markers[i],@legendpad+@legendlinelen//2,y-@legendfontsize//4,"",@legendfontsize)
          end 
          g.text(x: @legendpad+@legendfontsize+@legendlinelen,y: y ,text: @labels[i]) 
        end 
      end 
    end 

    def xaxis(min = @minx,max = @maxx,log = log)
      @svg.g(id: "xaxis",text_anchor: "middle") do |xaxis|
        xaxis.path(fill: "none") do |path|
          path.m(@padleft,@svg.height-@padbottom)
          path.l(@svg.width-@padright,@svg.height - @padbottom)
        end 

        t = SVG.transform do |t|
          t.translate(@svg.width//2,@svg.height-@axlabelfontsize)
        end 
        xaxis.g(id: "xlabel",transform: t) do |g| 
          g.text(text: @xlabel ,x: 0,y: 0,stroke_width: @axlabelstrokewidth)
        end 

        
        tickypos = @svg.height - @padbottom - @tickslen//2
        ticks = [{min,Plot.mapxcoords(min,min,max,@svg.width,@padleft,@padright,false)}]
        @ticksnum.times do |i|
          v = min + ((max - min)/(@ticksnum+1))*(i+1)
          ticks.push({v,Plot.mapxcoords(v,min,max,@svg.width,@padleft,@padright,false)})
        end 
        ticks.push({max,Plot.mapxcoords(max,min,max,@svg.width,@padleft,@padright,false)})

        # Dibujar los ticks 
        xaxis.path do |tickspath|
          ticks.each do |tick|
            tickspath.m(tick.last,tickypos)
            tickspath.l(0,@tickslen,relative: true)
          end 
        end 
        
        tickypos += @tickstextlen//2
        ticks.each do |tick|
          txt = tick.first.round(2).to_s
          txt = log ? "10^"+txt : txt 
          xaxis.text(
            x: tick.last,
            y: tickypos,
            text: txt,
            font_size: @ticksfontsize,
            stroke_width: @ticksstrokewidth)

        end 

      end 
    end 

    def yaxis(min = @miny,max = @maxy ,log = false)
      @svg.g(id: "yaxis") do |yaxis|
        yaxis.path(fill: "none") do |path|
          path.m(@padleft,@padtop)
          path.l(@padleft,@svg.height-@padbottom)
        end 

        # Mostrar el nombre
        t = SVG.transform do |t|
          t.translate(@axlabelfontsize,@svg.height//2)
          t.rotate(-90,0,0)
        end 
        yaxis.g(id: "yaxislabel",transform: t) do |g| 
          g.text(text: @ylabel ,x: 0,y: 0,stroke_width: @axlabelstrokewidth)
        end 


        # Obtener la posición de los ticks
        tickxpos = @padleft-@tickslen//2 
        ticks = [{min,Plot.mapycoords(min,min,max,@svg.height,@padtop,@padbottom,false)}]
        @ticksnum.times do |i|
          v = min + ((max - min) / (@ticksnum+1)) * (i+1)
          ticks.push({v,Plot.mapycoords(v,min,max,@svg.height,@padtop,@padbottom,false)})
        end 
        ticks.push({max,Plot.mapycoords(max,min,max,@svg.height,@padtop,@padbottom,false)})

        # Dibujar los ticks 
        yaxis.path do |tickspath|
          ticks.each do |tick|
            tickspath.m(tickxpos,tick.last)
            tickspath.l(@tickslen,0,relative: true)
          end 
        end 

        # Colocar los valores 
        tickxpos -= @tickstextlen//2
        ticks.each do |tick|
          txt = tick.first.round(2).to_s
          txt = log ? "10^"+txt : txt 
          yaxis.text(
            x: tickxpos, y: tick.last, 
            text: txt, 
            text_anchor: "middle",
            stroke_width: @ticksstrokewidth,
            font_size: @ticksfontsize)
        end 
      end 
    end 

    def axes(minx = @minx,maxx = @maxx,miny = @miny,maxy = @maxy,xlog = false,ylog = false)
      @svg.g(id: "axis" , stroke: @axcolor,fill: @axcolor,stroke_width: @axlinewidth) do |g|
        self.xaxis(minx,maxx,xlog)
        self.yaxis(miny,maxy,ylog)
      end
    end 

    def linestyle(style)
      case style
      when "-" then "5 0"
      when "--","dashed" then "5"
      when "-.","dashdot" then "6 3 3 3"
      when ":","dotted"  then "2"
      when " ","","none" then "0 1"
      else style
      end 
    end 

    def marker(marker,x,y,val,ms = @markersize)
      t = SVG.transform do |t|
        t.translate(x,y)
        t.scale(ms)
        case marker 
        when "D","x","X" then t.rotate(45)
        when ">","2","H" then t.rotate(90)
        when "v","3" then t.rotate(180)
        when "<","4" then t.rotate(270)
        else nil
        end 
      end 

      case marker 
      when "." then self.marker_point(t,val)
      when "," then self.marker_pixel(t,val)
      when "o" then self.marker_circle(t,val)
      when "^",">","v","<" then self.marker_triangle(t,val)
      when "s","D" then self.marker_square(t,val)
      when "d" then self.marker_diamond(t,val)
      when "+","x" then self.marker_plus(t,0.1,val)
      when "P","X" then self.marker_plus(t,0.2,val)
      when "1","2","3","4" then self.marker_tri(t,val)
      when "h","H" then self.marker_hexagon(t,val)
      when "p" then self.marker_pentagon(t,val)
      when "8" then self.marker_octagon(t,val)
      when "*" then self.marker_star(t,val)
      else raise "unkown marker #{marker}"
      end 
    end 
    
    def marker_point(t,val="")
      @svg.circle(cx: 0, cy: 0, r: 0.3, transform: t,title: val,stroke_width: 0)
    end 

    def marker_pixel(t,val="")
      @svg.circle(cx: 0, cy: 0, r: 0.2, transform: t,title: val,stroke_width: 0)
    end 

    def marker_circle(t,val="")
      @svg.circle(cx: 0, cy: 0, r: 0.45, transform: t,title: val,stroke_width: 0)
    end 
    
    def marker_triangle(t,val="")
      @svg.path(transform: t, title: val,stroke_width: 0) do |path|
        path.m(0,-0.5)
        path.l(0.5,0.5)
        path.l(-0.5,0.5)
        path.z
      end 
    end 

    def marker_square(t,val="")
      @svg.path(transform: t, title: val,stroke_width: 0) do |path|
        path.m(-0.4,-0.4)
        path.h(0.4)
        path.v(0.4)
        path.h(-0.4)
        path.z
      end 
    end 

    def marker_diamond(t,val="")
      @svg.path(transform: t, title: val,stroke_width: 0) do |path|
        path.m(0,-0.5)
        path.l(0.3,0)
        path.l(0,0.5)
        path.l(-0.3,0.0)
        path.z
      end 
    end 

    def marker_plus(t,width,val="")
      @svg.path(transform: t, title: val,stroke_width: 0 ) do |path|
        path.m(-width,-0.5)
        path.h(width)
        path.v(0.5)
        path.h(-width)
        path.z
      end 
      @svg.path(transform: t, title: val,stroke_width: 0 ) do |path|
        path.m(-0.5,-width)
        path.v(width)
        path.h(0.5)
        path.v(-width)
        path.z
      end 
    end 

    def marker_tri(t,val="")
      @svg.g(title: val, transform: t,stroke_width: 0) do |g|
        width = 0.1
        @svg.path(title: val) do |path|
          path.m(-width,0.5)
          path.h(width)
          path.v(0.0)
          path.h(-width)
          path.z
        end 
        @svg.path(
          title: val,
          transform: SVG.transform{|t| t.rotate(135)}) do |path|
          path.m(-width,0.5)
          path.h(width)
          path.v(0.0)
          path.h(-width)
          path.z
        end 
        @svg.path(
          title: val,
          transform: SVG.transform{|t| t.rotate(225)}) do |path|
          path.m(-width,0.5)
          path.h(width)
          path.v(0.0)
          path.h(-width)
          path.z
        end 
      end 
    end 
    
    def marker_hexagon(t,val="")
      @svg.path(title: val, transform: t,stroke_width: 0) do |path|
        path.m( 0.00,-0.50)
        path.l( 0.40,-0.20)
        path.l( 0.40, 0.30)
        path.l( 0.00, 0.50)
        path.l(-0.40, 0.30)
        path.l(-0.40,-0.20)
        path.z
      end 
    end 

    def marker_pentagon(t,val="")
      @svg.path(title: val, transform: t,stroke_width: 0) do |path|
        path.m( 0.00,-0.50)
        path.l( 0.45,-0.10)
        path.l( 0.30, 0.50)
        path.l(-0.30, 0.50)
        path.l(-0.45,-0.10)
        path.z
      end 
    end 

    def marker_octagon(t,val="")
      @svg.path(title: val, transform: t,stroke_width: 0) do |path|
        path.m(-0.25,-0.50)
        path.l( 0.18,-0.50)
        path.l( 0.50,-0.25)
        path.l( 0.50, 0.18)
        path.l( 0.18, 0.50)
        path.l(-0.25, 0.50)
        path.l(-0.50, 0.18)
        path.l(-0.50,-0.25)
        path.z
      end 
    end 

    def marker_star(t,val="")
      @svg.path(title: val, transform: t,stroke_width: 0) do |path|
        path.m(0,-0.5)
        path.l(-0.3,0.5)
        path.l(0.5,-0.1)
        path.l(-0.5,-0.1)
        path.l(0.3,0.5)
        path.z
      end 
    end 

  end 
end 
